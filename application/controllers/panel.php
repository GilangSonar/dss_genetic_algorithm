<?php
class Panel extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('m_custom');
    }

    function index(){
        if($this->session->userdata('logged_in') == TRUE){
            redirect('dashboard');
        };
        $data=array(
            'title'=>'Login Page'
        );
        $this->load->view('v_panel',$data);
    }

    function login() {
        $param['username'] = $this->input->post('username');
        $param['password'] = md5($this->input->post('password'));
        $param['status'] = 1;
        $user = $this->m_custom->getSelectData('tbl_member',$param)->result();
        if(!empty($user)){
            $session = array(
                'id_member'	=> $user[0]->user_id,
                'username'  => $user[0]->username,
                'status'    => $user[0]->status,
                'logged_in' => TRUE,
            );
            $this->session->set_userdata($session);
            redirect('dashboard');
        }else{
            $this->session->set_flashdata('alert','Login Failed');
            redirect('panel');
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}