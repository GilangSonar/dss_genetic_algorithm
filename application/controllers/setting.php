<?php
/**
 * Created by PhpStorm.
 * User: pethek
 * Date: 11/04/14
 * Time: 10:46
 */
class Setting extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('m_custom');
    }

//    ==============================================================
//      PARAMETER
//    ==============================================================
    function parameter (){
        $data=array(
            'title'=>'Setting-parameter',
            'act_setting'=>'act_setting',
            'display_setting'=>'display:block',
            'active_parameter'=>'background:#88c4e2',
            'dt_param'=>$this->m_custom->getAllData('tbl_parameter'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/setting/parameter/v_setting_parameter');
        $this->load->view('element/v_footer');
    }

    function update_parameter(){
        $id['id_parameter']=$this->input->post('id_parameter');
        $data=array(
            'populasi'=>$this->input->post('populasi'),
            'generasi'=>$this->input->post('generasi'),
            'kawin_silang'=>$this->input->post('kawin_silang'),
            'mutasi'=>$this->input->post('mutasi'),
            'elistism'=>$this->input->post('elistism'),
        );
        $this->m_custom->updateData('tbl_parameter',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('setting/parameter');
    }


//    ==============================================================
//      BOBOT
//    ==============================================================
    function bobot (){
        $data=array(
            'title'=>'Bobot Posisi Jabatan',
            'act_setting'=>'act_setting',
            'display_setting'=>'display:block',
            'active_bobot'=>'background:#88c4e2',

            'dt_posisi'=>$this->m_custom->getAllData('view_posisi'),
            'dt_unit_kerja'=>$this->m_custom->getAllData('tbl_unit_kerja'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/setting/bobot/v_bobot');
        $this->load->view('element/v_footer');
    }


    function bobot_posisi(){
        $id['id_posisi']=$this->uri->segment(4);
        $data=array(
            'title'=>'Setting Bobot Posisi Jabatan',
            'act_setting'=>'act_master',
            'display_setting'=>'display:block',
            'active_bobot'=>'background:#88c4e2',

            'dt_posisi'=>$this->m_custom->getSelectData('view_posisi',$id)->result(),
            'dt_kriteria'=>$this->m_custom->getAllData('view_kriteria'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/setting/bobot/v_add_bobot_posisi');
        $this->load->view('element/v_footer');
    }

    function input_bobot_posisi(){
        $id['id_posisi']=$this->input->post('id_posisi');
        $this->m_custom->deleteData('tbl_bobot_posisi',$id);

        $kriteria=$this->input->post('kriteria');
        $bobot=$this->input->post('bobot');
        foreach( $kriteria as $key => $id_kriteria ) {
            $data=array(
                'id_posisi'=>$this->input->post('id_posisi'),
                'id_kriteria'=>$id_kriteria,
                'bobot'=>$bobot[$key],
            );
            $this->m_custom->insertData("tbl_bobot_posisi",$data);
        }
        $this->session->set_flashdata('success_update',true);
        redirect("setting/bobot");
    }
}