<?php
/**
 * Created by PhpStorm.
 * User: GilangSonar
 * Date: 12/17/13
 * Time: 10:50 AM
 */

class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('m_custom');
    }

    function index(){
        $data=array(
            'title'=>'Dashboard | SPK PENENTUAN POSISI JABATAN KEPENDIDIKAN & ADMINISTRASI DI STTA',
            'act_dashboard'=>'active',
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/dashboard/v_dashboard');
        $this->load->view('element/v_footer');
    }

}