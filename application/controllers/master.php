<?php
class Master extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('m_custom');
    }

//    ==============================================================
//      PEGAWAI
//    ==============================================================
    function pegawai (){
        $data=array(
            'title'=>'pegawai',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_pegawai'=>'background:#88c4e2',

            'id_pegawai'=>$this->m_custom->getKodePegawai(),
            'dt_pegawai'=>$this->m_custom->getAllData('view_pegawai'),
            'dt_unit_kerja'=>$this->m_custom->getAllData('tbl_unit_kerja'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/master/pegawai/v_pegawai');
        $this->load->view('element/v_footer');
    }
    function add_pegawai(){
        $data=array(
            'title'=>'Tambah Karyawan',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_pegawai'=>'background:#88c4e2',

            'id_pegawai'=>$this->m_custom->getKodePegawai(),
            'dt_posisi'=>$this->m_custom->getAllData('tbl_posisi'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/master/pegawai/v_add_pegawai');
        $this->load->view('element/v_footer');
    }
    function view_pegawai(){
        $id['id_pegawai']=$this->uri->segment(3);
        $data=array(
            'title'=>'Setting Skor Pegawai',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_pegawai'=>'background:#88c4e2',

            'dt_pegawai'=>$this->m_custom->getSelectData('view_pegawai',$id)->result(),
            'dt_kriteria'=>$this->m_custom->getAllData('view_kriteria'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/master/pegawai/v_view_pegawai');
        $this->load->view('element/v_footer');
    }
    function edit_pegawai(){
        $id['id_pegawai']=$this->uri->segment(3);
        $data=array(
            'title'=>'Edit Pegawai',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_pegawai'=>'background:#88c4e2',

            'dt_pegawai'=>$this->m_custom->getSelectData('view_pegawai',$id)->result(),
            'dt_posisi'=>$this->m_custom->getAllData('tbl_posisi'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/master/pegawai/v_edit_pegawai');
        $this->load->view('element/v_footer');
    }
    function input_pegawai(){
        $data=array(
            'id_pegawai'=>$this->input->post('id_pegawai'),
            'NIP'=>$this->input->post('NIP'),
            'nm_pegawai'=>$this->input->post('nm_pegawai'),
            'umur'=>$this->input->post('umur'),
            'alamat'=>$this->input->post('alamat'),
            'id_posisi'=>$this->input->post('id_posisi'),
        );
        $this->m_custom->insertData('tbl_pegawai',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("master/pegawai");
    }
    function update_pegawai(){
        $id['id_pegawai']=$this->input->post('id_pegawai');
        $data=array(
            'NIP'=>$this->input->post('NIP'),
            'nm_pegawai'=>$this->input->post('nm_pegawai'),
            'umur'=>$this->input->post('umur'),
            'alamat'=>$this->input->post('alamat'),
            'id_posisi'=>$this->input->post('id_posisi'),
        );
        $this->m_custom->updateData('tbl_pegawai',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect("master/pegawai");
    }
    function delete_pegawai(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('master/pegawai');
        }else{
            $this->m_custom->deletePegawaiList($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('master/pegawai');
    }
    function skor_pegawai(){
        $id['id_pegawai']=$this->uri->segment(4);
        $data=array(
            'title'=>'Setting Skor Pegawai',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_pegawai'=>'background:#88c4e2',

            'dt_pegawai'=>$this->m_custom->getSelectData('view_pegawai',$id)->result(),
            'dt_kriteria'=>$this->m_custom->getAllData('view_kriteria'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/master/pegawai/v_add_skor_pegawai');
        $this->load->view('element/v_footer');
    }
    function input_skor_pegawai(){
        $id['id_pegawai']=$this->input->post('id_pegawai');
        $this->m_custom->deleteData('tbl_skor_pegawai',$id);

        $kriteria=$this->input->post('kriteria');
        $skor=$this->input->post('skor');
        foreach( $kriteria as $key => $id_kriteria ) {
            $data=array(
                'id_pegawai'=>$this->input->post('id_pegawai'),
                'id_kriteria'=>$id_kriteria,
                'skor'=>$skor[$key],
            );
            $this->m_custom->insertData("tbl_skor_pegawai",$data);
        }
        $this->session->set_flashdata('success_update',true);
        redirect("master/pegawai");
    }

//    ==============================================================
//      POSISI JABATAN
//    ==============================================================
    function posisi (){
        $data=array(
            'title'=>'Daftar Posisi Jabatan',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_posisi'=>'background:#88c4e2',

            'id_posisi'=>$this->m_custom->getKodePosisi(),
            'dt_posisi'=>$this->m_custom->getAllData('view_posisi'),
            'dt_unit_kerja'=>$this->m_custom->getAllData('tbl_unit_kerja'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/master/posisi/v_posisi');
        $this->load->view('element/v_footer');
    }
    function input_posisi(){
        $data=array(
            'id_posisi'=>$this->input->post('id_posisi'),
            'nm_posisi'=>$this->input->post('nm_posisi'),
            'id_unit'=>$this->input->post('id_unit'),
        );
        $this->m_custom->insertData('tbl_posisi',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("master/posisi");
    }
    function edit_posisi(){
        $id['id_posisi']=$this->input->post('id_posisi');
        $data=array(
            'nm_posisi'=>$this->input->post('nm_posisi'),
            'id_unit'=>$this->input->post('id_unit'),
        );
        $this->m_custom->updateData('tbl_posisi',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('master/posisi');
    }
    function delete_posisi(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('master/posisi');
        }else{
            $this->m_custom->deletePosisiList($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('master/posisi');
    }

//    ==============================================================
//      KRITERIA 
//    ==============================================================
    function kriteria(){
        $data=array(
            'title'=>'Kriteria Penentuan Keputusan',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_kriteria'=>'background:#88c4e2',

            'id_kriteria'=>$this->m_custom->getKodeKriteria(),
            'dt_kriteria'=>$this->m_custom->getAllData('view_kriteria'),
            'dt_unit_kerja'=>$this->m_custom->getAllData('tbl_unit_kerja'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/master/kriteria/v_kriteria');
        $this->load->view('element/v_footer');
    }
    function input_kriteria(){
        $data=array(
            'id_kriteria'=>$this->input->post('id_kriteria'),
            'nm_kriteria'=>$this->input->post('nm_kriteria'),
            'id_unit'=>$this->input->post('id_unit'),
        );
        $this->m_custom->insertData('tbl_kriteria',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("master/kriteria");
    }
    function edit_kriteria(){
        $id['id_kriteria']=$this->input->post('id_kriteria');
        $data=array(
            'nm_kriteria'=>$this->input->post('nm_kriteria'),
            'id_unit'=>$this->input->post('id_unit'),
        );
        $this->m_custom->updateData('tbl_kriteria',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('master/kriteria');
    }
    function delete_kriteria(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('master/kriteria');
        }else{
            $this->m_custom->deleteKriteria($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('master/kriteria');
    }

//    ==============================================================
//      MEMBER
//    ==============================================================
    function member (){
        $data=array(
            'title'=>'Member',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_member'=>'background:#88c4e2',
            'dt_member'=>$this->m_custom->getAllData('tbl_member'),
            'id_member'=>$this->m_custom->getKodeMember(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/master/member/v_member');
        $this->load->view('element/v_footer');
    }
    function input_member(){
        $data=array(
            'id_member'=>$this->input->post('id_member'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'status'=>$this->input->post('status'),
        );
        $this->m_custom->insertData('tbl_member',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("master/member");
    }
    function edit_member(){
        $id['id_member']=$this->input->post('id_member');
        $data=array(
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'status'=>$this->input->post('status'),
        );
        $this->m_custom->updateData('tbl_member',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('master/member');
    }
    function delete_member(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('master/member');
        }else{
            $this->m_custom->deleteMemberList($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('master/member');
    }

//    ==============================================================
//      UNIT KERJA
//    ==============================================================
    function unit_kerja(){
        $data=array(
            'title'=>'Daftar Posisi Jabatan',
            'act_master'=>'act_master',
            'display_master'=>'display:block',
            'active_unit'=>'background:#88c4e2',

            'id_unit'=>$this->m_custom->getKodeUnit(),
            'dt_unit_kerja'=>$this->m_custom->getAllData('tbl_unit_kerja'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('subelement/v_notification');
        $this->load->view('pages/master/unit_kerja/v_unit_kerja');
        $this->load->view('element/v_footer');
    }
    function input_unit_kerja(){
        $data=array(
            'id_unit'=>$this->input->post('id_unit'),
            'unit_kerja'=>$this->input->post('unit_kerja'),
        );
        $this->m_custom->insertData('tbl_unit_kerja',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("master/unit_kerja");
    }
    function edit_unit_kerja(){
        $id['id_unit']=$this->input->post('id_unit');
        $data=array(
            'unit_kerja'=>$this->input->post('unit_kerja'),
        );
        $this->m_custom->updateData('tbl_unit_kerja',$data,$id);
        $this->session->set_flashdata('success_update',true);
        redirect('master/unit_kerja');
    }
    function delete_unit_kerja(){
        $checked = $this->input->post('cek');
        if($checked == null){
            $this->session->set_flashdata('failed_delete',true);
            redirect('master/unit_kerja');
        }else{
            $this->m_custom->deleteUnitKerja($checked);
        }
        $this->session->set_flashdata('success_delete',true);
        redirect('master/unit_kerja');
    }
}