<?php
error_reporting(0);
class individual{
    // ------ Declare variables ------
    var $fitness_value;
    var $gene_value;
    var $chrom;

    // ------ Setting individual value ------
    function set($g_val,$f_val,$c_val){
        $this->fitness_value  = $f_val;
        $this->gene_value     = $g_val;
        $this->chrom          = $c_val;
    }
}

class GA{
    // ------ Declare variables ------
    var $population;
    var $totalGeneration;
    var $crossPoint;
    var $mutatePoint;
    var $elisPoint;
    var $arrayDataSeleksi;
    var $arr_individual;
    var $currentGeneration;

    // ------ Set Default Value of Variable ------
    function GA(){
        $this->currentGeneration = 0;
    }
    // ------ Employe data from selection method ------
    function dataEmployee(){
        $result = $this->arrayDataSeleksi;
        return ($result);
    }
    // ------ Shuffle data / record ------
    function shuffleData($arr){
        shuffle($arr);
        $len = count($arr);

        $array = array();
        for ($i=0; $i<$len;$i++){
            $array[$i]= $arr[$i] ;
        }
        return $array;
    }
    // ------ Fitness fuction : Calculate fitness of employee data record ------
    function fitness($data){
        $return = 0;
        $num_selected = count($data);
        for($i = 0; $i < $num_selected; $i++) {
            $return += ($num_selected - $i) * $data[$i];
        }
        $fit = round($return /= $num_selected,2);
        return $fit;
    }
    // ------ Build first generation of Genethic Algorithm ------
    function buildFirstGeneration(){
        for ($i=1 ; $i<=$this->population ; $i++)
        {
            $indiv = new individual();
            $g_value = $this->shuffleData($this->dataEmployee());
            $f_value = $this->fitness($g_value);
            $indiv->set($g_value,$f_value,implode(",",$g_value));
            $this->arr_individual[$i]=$indiv;
        }
        $this->currentGeneration++;
    }

}

class Proses_ga extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('m_custom');
    }
    
    // ============= // FUNCTION : Starting GA - Build First Generation // =============
    function index(){
        $CI =& get_instance();
        $CI->load->database();
        
        // ------ Truncate table if any record ------
        $tempGene   = $this->m_custom->getAllData('_temp_generation');
        $tempPC     = $this->m_custom->getAllData('_temp_parent_crossover');
        $tempPM     = $this->m_custom->getAllData('_temp_parent_mutate');
        $tempPE     = $this->m_custom->getAllData('_temp_parent_elistism');
        $randC      = $this->m_custom->getAllData('_temp_rand_crossover');
        $randM      = $this->m_custom->getAllData('_temp_rand_mutate');
        $randE      = $this->m_custom->getAllData('_temp_rand_elistism');
        if(!empty($tempGene)):
            $this->m_custom->truncate('_temp_generation');
        endif;
        if(!empty($tempPC)):
            $this->m_custom->truncate('_temp_parent_crossover');
        endif;
        if(!empty($tempPM)):
            $this->m_custom->truncate('_temp_parent_mutate');
        endif;
        if(!empty($tempPE)):
            $this->m_custom->truncate('_temp_parent_elistism');
        endif;
        if(!empty($randC)):
            $this->m_custom->truncate('_temp_rand_crossover');
        endif;
        if(!empty($randM)):
            $this->m_custom->truncate('_temp_rand_mutate');
        endif;
        if(!empty($randE)):
            $this->m_custom->truncate('_temp_rand_elistism');
        endif;

        // ------ Declare & set GA variables ------
        $ga = new GA();
        $ga->totalGeneration 	= $this->input->post('generation');
        $ga->population			= $this->input->post('population');
        $ga->crossPoint     	= $this->input->post('crossover');
        $ga->mutatePoint    	= $this->input->post('mutate');
        $ga->elisPoint 	        = $this->input->post('elistism');
        $ga->arrayDataSeleksi   = $this->dataSeleksi();

        // ====== DEBUGGING ! ======
        for($i=($ga->currentGeneration+1) ; $i<=($ga->totalGeneration) ; $i++){
            $ga->buildFirstGeneration();
            $this->showThisGeneration($ga->arr_individual,$i);

            $selectPC = $this->selectParentCross($i-1);
            foreach($selectPC as $key => $row){
                $pc = array(
                    'generation'    => $i,
                    'no'            => $key,
                    'chromosome'    => $row
                );
                $this->m_custom->insertData('_temp_parent_crossover',$pc);
            }
            $selectPM = $this->selectParentMutate($i-1);
            foreach($selectPM as $key => $row){
                $pm = array(
                    'generation'    => $i,
                    'no'            => $key,
                    'chromosome'    => $row
                );
                $this->m_custom->insertData('_temp_parent_mutate',$pm);
            }
            $selectPE = $this->selectParentElistism($i-1);
            foreach($selectPE as $key => $row){
                $pe = array(
                    'generation'    => $i,
                    'no'            => $key,
                    'chromosome'    => $row
                );
                $this->m_custom->insertData('_temp_parent_elistism',$pe);
            }
        }

        $res = $this->m_custom->manualQuery('select * from _temp_generation order by generation ASC');
        $data_gene = array_chunk($res,$ga->population);

        // echo "<pre>";print_r($data_gene);echo "</pre>";die();

        $data=array(
            'dt_kandidat'   => $this->m_custom->getAllTempSeleksi(),
            'dt_gene'       => $data_gene,
            'dt_cross'      => $this->m_custom->getAllData('_temp_parent_crossover'),
            'paramC'        => $ga->crossPoint,
            'paramM'        => $ga->mutatePoint,
            'paramE'        => $ga->elisPoint,
            'randC'         => $this->m_custom->manual("select * from _temp_rand_crossover")->result_array(),
            'randM'         => $this->m_custom->manual("select * from _temp_rand_mutate")->result_array(),
            'randE'         => $this->m_custom->manual("select * from _temp_rand_elistism")->result_array(),
            'parentC'       => $this->m_custom->manual("select * from _temp_parent_crossover where chromosome != ' '")->result_array(),
            'parentM'       => $this->m_custom->manual("select * from _temp_parent_mutate where chromosome != ' '")->result_array(),
            'parentE'       => $this->m_custom->manual("select * from _temp_parent_elistism where chromosome != ' '")->result_array(),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/proses/v_result');
        $this->load->view('element/v_footer');

    }


// ##############################################################################################
//                                FUNCTION FOR GA
// ##############################################################################################

    // ======== FUNCTION for creating random number ========
    function createNum(){
        $string = "0." ;
        for($i=1;$i<=3;$i++)
            $string .= floatval(rand(0,9));
        $num = $string;
        return ($num);
    }

    // ======== FUNCTION : getting employee data (total point) from first selection method ========
    function dataSeleksi(){
        $dt_seleksi = $this->m_custom->manual('select total from _temp_seleksi');
        if ($dt_seleksi->num_rows() > 0){
            $data = array();
            $i = 0;
            foreach ($dt_seleksi->result() as $row){
                foreach ($row as $value){
                    $data[$i]= $value;
                }
                $i++;
            }
        }
        return $data;
    }

    // ======== FUNCTION : getting generation data used for selectParent function ========
    function dataGene($currentGene){
        $dt_gene = $this->m_custom->manual("select chromosome from _temp_generation where generation = '$currentGene'");
        if ($dt_gene->num_rows() > 0){
            $data = array();
            $i = 0;
            foreach ($dt_gene->result() as $row){
                foreach ($row as $value){
                    $data[$i]= $value;
                }
                $i++;
            }
        }
        return $data;
    }


    // ======= FUNCTION : Show Generation & Insert generation data {no_pop,chromosome,fitness,prob,kom.prob,id} ======
    // ------ 'id' : sequence of generation -----
    function showThisGeneration($arr_val,$gene){
        $dt_cross = $this->m_custom->manual('select * from tbl_parameter')->result();
        foreach($dt_cross as $row): $pop = $row->populasi; endforeach;

        $sumFitness = 0;
        for($i=1 ; $i<=$pop ; $i++){
            $sumFitness += $arr_val[$i]->fitness_value;
        }

        for($i=1;$i<=$pop;$i++){
            $probability = round($arr_val[$i]->fitness_value / $sumFitness,3);

            if($i < 2 ){
                $komprob = $probability;
            }else{
                $komprob += $probability;
            }
            $data = array(
                'generation'=> $gene,
                'no_pop'    => $i,
                'chromosome'=> $arr_val[$i]->chrom,
                'fitness'   => $arr_val[$i]->fitness_value,
                'prob'      => $probability,
                'komprob'   => $komprob,
            );
            $this->m_custom->insertData('_temp_generation',$data);
        }
    }

// ===================== FOR CROSSOVER =============================

    // ======= FUNCTION : Selecting parent for crossover method by comparing random number and generation data =======
    function selectParentCross($currentGene){
        $CI =& get_instance();
        $CI->load->database();

        // ---- Declare param from database ----
        $dt_param = $this->m_custom->manual('select * from tbl_parameter')->result();
        foreach($dt_param as $row):
            $pop = $row->populasi ;
            $param_crossover = $row->kawin_silang / 100;
        endforeach;

        // ---- Creating random number ----
        for($i=0; $i < $pop; $i++){
            $n[]=$this->createNum();
        }

        // ---- Insert random number into database ----
        $data=array();
        $data_array=array();
        $i = 0;
        foreach($n as $key =>$row){
            $data_rand=array(
                'id'=>$key,
                'number'=>$row,
                'no_gene'=>$currentGene+1
            );
            $CI->db->INSERT('_temp_rand_crossover',$data_rand);
            $data[$key] = $row;
        }

        // ---- Selecting data choosed from temp generation ----
        foreach($data as $key => $val){
            if($val <= $param_crossover ): // ---- Compare with param crossover ---
                $data_array[$i]=$this->dataGene($currentGene)[$key];
                $i++;
            endif;
        }
        return ($data_array);
    }

// ===================== FOR MUTATE =============================

    // ======= FUNCTION : Selecting parent for crossover method by comparing random number and generation data =======
    function selectParentMutate($currentGene){
        $CI =& get_instance();
        $CI->load->database();

        // ---- Declare param from database ----
        $dt_param = $this->m_custom->manual('select * from tbl_parameter')->result();
        foreach($dt_param as $row):
            $pop = $row->populasi ;
            $param_mutate = $row->mutasi / 100;
        endforeach;

        // ---- Creating random number ----
        for($i=0; $i < $pop; $i++){
            $n[]=$this->createNum();
        }

        // ---- Insert random number into database ----
        $data=array();
        $data_array=array();
        $i = 0;
        foreach($n as $key =>$row){
            $data_rand=array(
                'id'=>$key,
                'number'=>$row,
                'no_gene'=>$currentGene+1
            );
            $CI->db->INSERT('_temp_rand_mutate',$data_rand);
            $data[$key] = $row;
        }

        // ---- Selecting data choosed from temp generation ----
        foreach($data as $key => $val){
            if($val <= $param_mutate ): // ---- Compare with param mutate ---
                $data_array[$i]=$this->dataGene($currentGene)[$key];
                $i++;
            endif;
        }
        return ($data_array);
    }

// ===================== FOR ELISTISM =============================

    // ======= FUNCTION : Selecting parent for crossover method by comparing random number and generation data =======
    function selectParentElistism($currentGene){
        $CI =& get_instance();
        $CI->load->database();

        // ---- Declare param from database ----
        $dt_param = $this->m_custom->manual('select * from tbl_parameter')->result();
        foreach($dt_param as $row):
            $pop = $row->populasi ;
            $param_elistism = $row->elistism / 100;
        endforeach;

        // ---- Creating random number ----
        for($i=0; $i < $pop; $i++){
            $n[]=$this->createNum();
        }

        // ---- Insert random number into database ----
        $data=array();
        $data_array=array();
        $i = 0;
        foreach($n as $key =>$row){
            $data_rand=array(
                'id'=>$key,
                'number'=>$row,
                'no_gene'=>$currentGene+1
            );
            $CI->db->INSERT('_temp_rand_elistism',$data_rand);
            $data[$key] = $row;
        }

        // ---- Selecting data choosed from temp generation ----
        foreach($data as $key => $val){
            if($val <= $param_elistism ): // ---- Compare with param elistism ---
                $data_array[$i]=$this->dataGene($currentGene)[$key];
                $i++;
            endif;
        }
        return ($data_array);
    }
}