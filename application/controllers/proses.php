<?php

class Proses extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
            $this->session->set_flashdata('alert','Silahkan login terlebih dahulu.');
            redirect(base_url());
        };
        $this->load->model('m_custom');
    }

    function seleksi($unit_kerja){
        $tempSeleksi = $this->m_custom->getAllData('_temp_seleksi');
        if(!empty($tempSeleksi)){
            $this->db->empty_table('_temp_seleksi');
        }
        $id['id_unit']= $unit_kerja;
        $data=array(
            'title'=>'Seleksi Posisi Jabatan Administrasi',
            'act_proses'=>'act_active',
            'display_proses'=>'display:block',

            'dt_posisi'=>$this->m_custom->getSelectData('view_posisi',$id)->result()
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/proses/seleksi/v_seleksi');
        $this->load->view('element/v_footer');
    }

    function run_seleksi(){
        $unit_kerja= $this->uri->segment(3);
        $idPosisi['id_posisi']=$this->uri->segment(4);
        $data=array(
            'title'=>'Seleksi Posisi Jabatan Administrasi',
            'act_proses'=>'act_active',
            'display_proses'=>'display:block',

            'dt_posisi'=>$this->m_custom->getSelectData('view_posisi',$idPosisi)->result(),
            'dt_pegawai'=>$this->m_custom->getPegawaiNoSelect($unit_kerja),
            'dt_temp_seleksi'=>$this->m_custom->getTempSeleksi($this->uri->segment(4)),
            'param'=>$this->m_custom->getAllData('tbl_parameter'),
        );
        $this->load->view('element/v_header',$data);
        $this->load->view('pages/proses/seleksi/v_run_seleksi');
        $this->load->view('element/v_footer');
    }

    function temp(){
        $unit=$this->uri->segment(3);
        $pegawai=$this->uri->segment(4);
        $posisi=$this->uri->segment(5);

        foreach($this->m_custom->getBobotPosisi($posisi) as $row) {
            $arrayDataBobot[] =  $row->bobot;
            $arrayDataKriteria[] = $row->id_kriteria;
        }
        foreach($this->m_custom->getSkorPegawai($pegawai) as $row) {
            $arrayDataSkor[] =  $row->skor;
        }
        $arrayKriteria = $arrayDataKriteria;
        $arrayBobot=$arrayDataBobot;
        $arraySkor=$arrayDataSkor;
        $length=count($arrayKriteria);
        $i=0;
        $total=0;
        for($j=0;$j<$length;$j++) {
            $total = $total + $arrayBobot[$i] * $arraySkor[$i];
            $i++;
        }
        $data=array(
            'id_temp'=>1,
            'id_posisi'=>$posisi,
            'id_pegawai'=>$pegawai,
            'total'=>$total
        );
        $this->m_custom->insertData('_temp_seleksi',$data);
        $this->session->set_flashdata('success_add',true);
        redirect("proses/run_seleksi/".$unit.'/'.$posisi,'refresh');
    }

    function deleteTemp(){
        $unit=$this->uri->segment(3);
        $posisi=$this->uri->segment(4);
        $id['id_pegawai']=$this->uri->segment(5);

        $this->m_custom->deleteData('_temp_seleksi',$id);
        $this->session->set_flashdata('success_add',true);
        redirect("proses/run_seleksi/".$unit.'/'.$posisi,'refresh');
    }

}