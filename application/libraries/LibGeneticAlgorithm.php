<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 12/11/14
 * Time: 23:59
 */
class individual
{
    var $fitness_value;
    var $gene_value;
    var $chrom;

    function set($g_val,$f_val,$c_val){
        $this->fitness_value  = $f_val;
        $this->gene_value     = $g_val;
        $this->chrom          = $c_val;
    }
}

class GA
{
    var $population;
    var $totalGeneration;
    var $crossPoint;
    var $mutatePoint;
    var $elisPoint;
    var $arrayData;
    var $arr_individual;
    var $arr_offspring;
    var $currentGeneration;
    var $arr_avefitness ;
    var $counter_arr_avefitness;
    var $probalility;

    function GA()
    {
        $this->currentGeneration = 0;
        $this->counter_arr_avefitness = 0;
        $this->counter_arr_probability = 0;
    }

    function dataEmp()
    {
        $result = $this->arrayData;
        return ($result);
    }

    function shuffleData($arr){
        shuffle($arr);
        $len = count($arr);

        $array = array();
        for ($i=0; $i<$len;$i++){
            $array[$i]= $arr[$i] ;
        }
        return $array;
    }

    function fitness($data){
        $return = 0;
        $num_selected = count($data);
        for($i = 0; $i < $num_selected; $i++) {
            if($i < 2)
                $return += ($num_selected - $i) * $data[$i];
            else
                $return += $data[$i];
        }
        $fit = round($return /= $num_selected,2);
        return $fit;
    }

    function buildFirstGeneration()
    {
        for ($i=1 ; $i<=$this->population ; $i++)
        {
            $indiv = new individual();
            $g_value = $this->shuffleData($this->dataEmp());
            $f_value = $this->fitness($g_value);
            $indiv->set($g_value,$f_value,implode(",",$g_value));
            $this->arr_individual[$i]=$indiv;
        }
        $this->currentGeneration++;
    }

    function parentGeneration($arr_val){
        $CI =& get_instance();
        $CI->load->database();

        $pop = "";
        $dt_cross = $this->m_custom->manual('select * from tbl_parameter')->result();
        foreach($dt_cross as $row): $pop = $row->populasi ; endforeach;

        $sumFitness = 0;
        for($i=1 ; $i<=$pop ; $i++)
        {
            $sumFitness += $arr_val[$i]->fitness_value;
        }

        // ----------- Get Prob, Kom.Prob & Input temp_generation --------------
        for($i=1;$i<=$pop;$i++)
        {
            $probability = round($arr_val[$i]->fitness_value / $sumFitness,3);
            $komprob = 0;
            if($i < 2 ){
                $komprob = $probability;
            }else{
                $komprob += $probability;
            }
            $data = array(
                'id'=>1,
                'no_pop' => $i,
                'chromosome'=>$arr_val[$i]->chrom,
                'fitness'=>$arr_val[$i]->fitness_value,
                'prob'=>$probability,
                'komprob'=>$komprob,
            );
            $this->m_custom->insertData('_temp_generation',$data);
        }

        // ----------- Kawin Silang - Mutasi - Pelestarian ---------
        // Hapus temporary data _temp_crossover jika ada
        $tempTemp = $this->m_custom->getAllData('_temp_crossover');
        if(!empty($tempTemp)){
            $id['id'] = 1;
            $this->m_custom->deleteData('_temp_crossover',$id);

            // Select kromosom untuk proses Crossover dr tabel _temp_generation. ($this->selectCrossover)
            // Prosesnya = membuat array random angka dgn range 0,00 s/d 1,00.
            // Angka random yang dipilih adalah yg tidak melebihi parameter yg ditentukan.
            // Select data kromosom dr tabel _temp_generation, dimana no urut array kromosom = no urut array angka random yg terpilih.
            // Selenjutnya kromosom yang sudah di select, di insert kan kedalam table _temp_crossover untuk kebutuhan proses selanjutnya.
            $selCross = $this->selectCrossover();
            foreach($selCross as $key => $row){
                $r = array(
                    'id'        => 1,
                    'no'        => $key,
                    'kromosom'  => $row
                );
                $this->m_custom->insertData('_temp_crossover',$r);
            }
        }else{
            $selCross = $this->selectCrossover();
            foreach($selCross as $key => $row){
                $r = array(
                    'id'        => 1,
                    'no'        => $key,
                    'kromosom'  => $row
                );
                $this->m_custom->insertData('_temp_crossover',$r);
            }
        }

        // Get data temp_crossover,
        // lalu di lakukan kawin silang antar crossover.
        // Metode : Kawin silang banyak titik.
        $this->session->unset_userdata('kromosom1');
        $this->session->unset_userdata('kromosom2');

        $temp_cross = $this->m_custom->manual('select * from _temp_crossover limit 2');
        if($temp_cross->num_rows() > 1 ){
            $result = $temp_cross->result_array();
            $data_result = array(
                'kromosom1' => $result[0]['kromosom'],
                'kromosom2' => $result[1]['kromosom'],
            );
            $this->session->set_userdata($data_result);
            $krom1 = $this->session->userdata('kromosom1');
            $krom2 = $this->session->userdata('kromosom2');

            $temp_gene = $this->m_custom->manual("
                select no_pop,chromosome from _temp_generation
                where chromosome = '$krom1' OR chromosome = '$krom2'
                ")->result_array();

            $temp_gene2 = $this->m_custom->manual('select * from _temp_generation')->result_array();

            if(!empty($temp_gene)):
                $this->m_custom->insertData('_temp_result_crossover',$temp_gene2);

//                    foreach($temp_gene as $row){
//                        $pop1 = $row['no_pop'][0];
//                        $pop2 = $row['no_pop'][1];
//                        $temp1 = $pop1;
//                        $pop1 = $pop2;
//                        $pop2 = $temp1;
//                    }
//                    $d = array($pop1,$pop2);
//                    echo "<pre>";
//                    print_r($d);
//                    echo "<pre> <hr/>";


                echo "<pre>";
                print_r($temp_gene);
                echo "<pre> <hr/>";

                echo "<pre>";
                print_r($temp_gene2);
                echo "<pre> <hr/>";
            endif;
        }


        die();
    }
}