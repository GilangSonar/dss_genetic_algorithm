<?php $this->load->view('subelement/v_sidebar') ?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar') ?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Setting</li>
            <span class="divider">/</span>
            <li class="active">Parameter</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Parameter</h1> <span>Setting Parameter Metode Algoritma Genetika</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <h4 class="widgettitle nomargin shadowed">Setting Parameter</h4>
            <div class="widgetcontent bordered shadowed ">

                <?php if(isset($dt_param)){ foreach($dt_param as $row) {?>
                    <form class="stdform" method="post" action="<?php echo site_url('setting/update_parameter')?>">
                        <input type="hidden" value="<?= $row->id_parameter?>" name="id_parameter"/>
                        <table class="table table-bordered mailinbox">
                            <colgroup>
                                <col class="con0"/>
                                <col class="con1"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th class="head0 center">Parameter</th>
                                <th class="head1 center span1">Bobot</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>Ukuran Populasi</td>
                                <td>
                                    <input name="populasi" class="form-control text-center " type="number" max="10" min="0" value="<?= $row->populasi?>" required=""/>
                                </td>
                            </tr>
                            <tr>
                                <td>Masksimum Generasi</td>
                                <td>
                                    <input name="generasi" class="form-control text-center" type="number" min="0" value="<?= $row->generasi?>" required=""/>
                                </td>
                            </tr>
                            <tr>
                                <td>Probabilitas Kawin Silang ( % )</td>
                                <td>
                                    <input name="kawin_silang" class="form-control text-center" type="number" max="100" min="0" value="<?= $row->kawin_silang?>" required=""/>
                                </td>
                            </tr>
                            <tr>
                                <td>Probabilitas Mutasi ( % )</td>
                                <td>
                                    <input name="mutasi" class="form-control text-center" type="number" max="100" min="0" value="<?= $row->mutasi?>" required=""/>
                                </td>
                            </tr>
                            <tr>
                                <td>Probabilitas Elistism ( % )</td>
                                <td>
                                    <input name="elistism" class="form-control text-center" type="number" max="100" min="0" value="<?= $row->elistism?>" required=""/>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="stdformbutton">
                            <button type="submit" class="btn btn-primary">Update Data</button>
                        </p>
                    </form>
                <?php } } ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>



