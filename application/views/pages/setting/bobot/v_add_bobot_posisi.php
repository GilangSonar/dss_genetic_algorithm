<?php $this->load->view('subelement/v_sidebar') ?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar') ?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Setting</li>
            <span class="divider">/</span>
            <li class="active">Bobot Kriteria</li>
            <span class="divider">/</span>
            <li class="active">Update Bobot</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Update Bobot</h1> <span>Update Nilai Bobot Kriteria Pada Setiap Posisi Jabatan</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <h4 class="widgettitle nomargin shadowed">Update Bobot Kriteria</h4>
            <div class="widgetcontent bordered shadowed ">
                <?php  if(isset($dt_posisi)){foreach ($dt_posisi as $rowData){ ?>
                    <form class="stdform" method="post" action="<?php echo site_url('setting/input_bobot_posisi')?>">
                        <div class="row-fluid">
                            <div class="span4 text-uc">
                                <?php if(isset($dt_posisi)){foreach($dt_posisi as $row) { ?>
                                    <input type="hidden" name="id_posisi" value="<?= $row->id_posisi ?>" readonly/>
                                    <dl class="dl-horizontal">
                                        <dt>ID Posisi Jabatan: </dt> <dd><?= $row->id_posisi?></dd>
                                        <dt>Posisi Jabatan: </dt> <dd><?= $row->nm_posisi?></dd>
                                        <dt>Unit Kerja: </dt> <dd><?= $row->unit_kerja?></dd>
                                    </dl>
                                <?php } } ?>
                                <hr/>
                                <div class="alert alert-info">
                                    <h3>KETERANGAN BOBOT ( 1 - 5 )</h3>
                                    <strong>Nilai Bobot 5 : SANGAT BAIK</strong><br/>
                                    <strong>Nilai Bobot 4 : BAIK</strong><br/>
                                    <strong>Nilai Bobot 3 : CUKUP</strong><br/>
                                    <strong>Nilai Bobot 2 : KURANG</strong><br/>
                                    <strong>Nilai Bobot 1 : SANGAT KURANG</strong>
                                </div>
                            </div>
                            <div class="span8">
                                <table class="table table-bordered mailinbox">
                                    <colgroup>
                                        <col class="con0"/>
                                        <col class="con1"/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th class="head0 center">Kriteria</th>
                                        <th class="head1 center span1">Skor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($dt_kriteria)){ foreach($dt_kriteria as $row) {
                                        if($row->id_unit == $rowData->id_unit){
                                            $param['id_kriteria']= $row->id_kriteria;
                                            $param['id_posisi']= $rowData->id_posisi;
                                            $skor = $this->m_custom->getSelectData('tbl_bobot_posisi',$param)->result();
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="kriteria[]" value="<?= $row->id_kriteria?>"/>
                                                    <?= $row->nm_kriteria?>
                                                </td>
                                                <td>
                                                    <input name="bobot[]" class="form-control text-center " type="number" max="5" min="0" value="<?php if(!empty($skor[0]->bobot)){ echo $skor[0]->bobot;}else{ echo '0';}?>" required=""/>
                                                </td>
                                            </tr>
                                        <?php } } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr/>
                        <button type="submit" class="btn btn-primary">Update Bobot</button>
                        <a href="<?= site_url('setting/bobot') ?>" class="btn"> Cancel </a>
                    </form>
                <?php } } ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>



