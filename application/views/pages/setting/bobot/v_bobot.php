<?php $this->load->view('subelement/v_sidebar') ?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar') ?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Setting</li>
            <span class="divider">/</span>
            <li class="active">Bobot Posisi Jabatan</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Bobot Posisi Jabatan</h1> <span>Nilai Bobot Posisi Jabatan</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                    <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                        <li class="ui-state-default ui-corner-top <?php if($i == 0 ) echo "ui-tabs-active ui-state-active"?>" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-<?= $row->id_unit?>" aria-selected="<?php if($i == 0 ) echo "true"?>">
                            <a href="#tabs-<?= $row->id_unit?>" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><?= $row->unit_kerja?></a>
                        </li>
                    <?php } } ?>
                </ul>

                <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                    <div id="tabs-<?= $row->id_unit?>" aria-labelledby="ui-id-<?= $row->id_unit?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
                        <table class="table table-bordered mailinbox dyntable">
                            <colgroup>
                                <col class="con1 width4" />
                                <col class="con0 width10"/>
                                <col class="con0 width10"/>
                                <col class="con0 width10"/>
                                <col class="con0 width4"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th class="head1 center"><input type="checkbox" name="checkall" class="checkall" /></th>
                                <th class="head0 center">Kode Posisi</th>
                                <th class="head0 center">Unit Kerja</th>
                                <th class="head0 center">Posisi Jabatan</th>
                                <th class="head0 center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($dt_posisi)){foreach ($dt_posisi as $row2){
                                if($row->id_unit == $row2->id_unit) { ?>
                                    <tr>
                                        <td class="center">
                                            <input type="checkbox" name="cek[]" value="<?php echo $row2->id_posisi?>"/>
                                        </td>
                                        <td><?php echo $row2->id_posisi?></td>
                                        <td class="text-uc"><?php echo $row2->unit_kerja?></td>
                                        <td><?php echo $row2->nm_posisi?></td>
                                        <td class="center">
                                            <a href="<?= site_url('setting/bobot_posisi/'.$row->id_unit.'/'.$row2->id_posisi)?>" class="btn btn-mini">
                                                <i class="icon-edit"></i> Setting Bobot</a>
                                        </td>
                                    </tr>
                                <?php } } }?>
                            </tbody>
                        </table>
                    </div>
                <?php } } ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>