<?php $this->load->view('subelement/v_sidebar') ?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar') ?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Master Data</li>
            <span class="divider">/</span>
            <li class="active">Kriteria</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Kriteria</h1> <span>Kriteria Penentuan Keputusan</span>
        <a href="#modalAdd" data-toggle="modal" class="pull-right btn-group btn btn-primary"><i class="icon-plus-sign icon-white"></i> Tambah Data</a>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <form method="post" action="<?= site_url('master/delete_kriteria')?>">
                <div class="msghead">
                    <ul class="msghead_menu">
                        <li class="right">
                            <button type="submit" class="btn" onclick="return confirm('Are you sure want to delete ?');">
                                <span class="iconsweets-trashcan"></span> Hapus Data
                            </button>
                        </li>

                    </ul>
                    <span class="clearall"></span>
                </div>
                <br>
                <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                    <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                        <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                            <li class="ui-state-default ui-corner-top <?php if($i == 0 ) echo "ui-tabs-active ui-state-active"?>" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-<?= $row->id_unit?>" aria-selected="<?php if($i == 0 ) echo "true"?>">
                                <a href="#tabs-<?= $row->id_unit?>" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><?= $row->unit_kerja?></a>
                            </li>
                        <?php } } ?>
                    </ul>
                    <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                        <div id="tabs-<?= $row->id_unit?>" aria-labelledby="ui-id-<?= $row->id_unit?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
                            <table class="table table-bordered mailinbox dyntable">
                                <colgroup>
                                    <col class="con1 width4" />
                                    <col class="con0 width10"/>
                                    <col class="con0 width10"/>
                                    <col class="con0 width10"/>
                                    <col class="con0 width4"/>
                                </colgroup>
                                <thead>
                                <tr>
                                    <th class="head1 center"><input type="checkbox" name="checkall" class="checkall" /></th>
                                    <th class="head0 center">Kode Kriteria </th>
                                    <th class="head0 center">Unit Kerja </th>
                                    <th class="head0 center">Kriteria </th>
                                    <th class="head0 center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($dt_kriteria)){foreach ($dt_kriteria as $row2){
                                    if($row->id_unit == $row2->id_unit) { ?>
                                        <tr>
                                            <td class="center">
                                                <input type="checkbox" name="cek[]" value="<?php echo $row2->id_kriteria?>"/>
                                            </td>
                                            <td><?php echo $row2->id_kriteria?></td>
                                            <td class="text-uc"><?php echo $row2->unit_kerja?></td>
                                            <td><?php echo $row2->nm_kriteria?></td>
                                            <td class="center">
                                                <a href="#modalEdit<?=$row2->id_kriteria?>" class="btn btn-mini btn-block" data-toggle="modal">
                                                    <i class="icon-edit"></i> Edit</a>
                                            </td>
                                        </tr>
                                    <?php } } } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } } ?>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!--======== MODAL ADD =================  -->
<form class="form" method="post" action="<?php echo site_url('master/input_kriteria')?>">
    <div aria-hidden="false" aria-labelledby="myModalLabel" class="modal hide fade in" id="modalAdd">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 id="myModalLabel">Tambah Data</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label" for="id_kriteria ">ID Kriteria </label>
                <div class="controls">
                    <input type="text" name="id_kriteria" class="input-xlarge" value="<?= $id_kriteria?>" readonly />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Unit Kerja</label>
                <div class="controls">
                    <select name="id_unit" class="text-uc">
                        <option value=""> Choose One ... </option>
                        <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $row){ ?>
                            <option class="text-uc" value="<?= $row->id_unit?>"> <?= $row->unit_kerja?> </option>
                        <?php } } ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Kriteria </label>
                <div class="controls">
                    <input type="text" name="nm_kriteria" class="input-xlarge"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn">Close</button>
            <button class="btn btn-primary">Save changes</button>
        </div>
    </div>
</form>

<!--=================== MODAL EDIT ========================-->
<?php  if(isset($dt_kriteria)){foreach ($dt_kriteria as $row){ ?>
    <form class="form" method="post" action="<?php echo site_url('master/edit_kriteria')?>">
        <div aria-hidden="false" aria-labelledby="myModalLabel" class="modal hide fade in" id="modalEdit<?=$row->id_kriteria?>">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h3 id="myModalLabel">Edit Data</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label">ID Kriteria </label>
                    <div class="controls">
                        <input type="text" name="id_kriteria" class="input-xlarge" value="<?= $row->id_kriteria ?>" readonly />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Unit Kerja</label>
                    <div class="controls">
                        <select name="id_unit" class="text-uc">
                            <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $row2){ ?>
                                <option class="text-uc" value="<?= $row2->id_unit?>" <?php if($row2->unit_kerja == $row->unit_kerja) echo "selected"?>> <?= $row2->unit_kerja?> </option>
                            <?php } } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Kriteria </label>
                    <div class="controls">
                        <input type="text" name="nm_kriteria" class="input-xlarge" value="<?= $row->nm_kriteria?>" required=""/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </form>
<?php } } ?>
