<div class="hidden-print">
    <?php $this->load->view('subelement/v_sidebar') ?>
    <div class="rightpanel">
        <?php $this->load->view('subelement/v_topbar') ?>
        <div class="breadcrumbwidget">
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
                <li class="active">Master Data</li>
                <span class="divider">/</span>
                <li class="active">Pegawai</li>
            </ul>
        </div>
        <div class="pagetitle">
            <h1>Karyawan</h1> <span>Daftar pegawai</span>
            <a href="<?= site_url('master/add_pegawai')?>"
               class="pull-right btn-group btn btn-primary"><i class="icon-plus-sign icon-white"></i> Tambah Data
            </a>
        </div>
        <div class="maincontent">
            <div class="contentinner">
                <form method="post" action="<?= site_url('master/delete_pegawai')?>">
                    <div class="msghead">
                        <ul class="msghead_menu">
                            <li class="right">

                                <a href="" class="btn btn-danger" onclick="window.print()">
                                    <i class="icon-white icon-print"></i> Print</a>&nbsp;
                                <button type="submit" class="btn" onclick="return confirm('Are you sure want to delete ?');">
                                    <span class="iconsweets-trashcan"></span> Hapus Data
                                </button>
                            </li>
                        </ul>
                        <span class="clearall"></span>
                    </div>
                    <br>
                    <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                            <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                                <li class="ui-state-default ui-corner-top <?php if($i == 0 ) echo "ui-tabs-active ui-state-active"?>" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-<?= $row->id_unit?>" aria-selected="<?php if($i == 0 ) echo "true"?>">
                                    <a href="#tabs-<?= $row->id_unit?>" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1"><?= $row->unit_kerja?></a>
                                </li>
                            <?php } } ?>
                        </ul>
                        <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $i=>$row){ ?>
                            <div id="tabs-<?= $row->id_unit?>" aria-labelledby="ui-id-<?= $row->id_unit?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
                                <table class="table table-bordered mailinbox dyntable">
                                    <colgroup>
                                        <col class="con1 width4" />
                                        <col class="con0 width5"/>
                                        <col class="con1 width10"/>
                                        <col class="con0 width4"/>
                                        <col class="con0 width10"/>
                                        <col class="con1 width4"/>
                                        <col class="con0 width4"/>
                                        <col class="con0 width15"/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th class="head1 center"><input type="checkbox" name="checkall" class="checkall" /></th>
                                        <th class="head0 center">NIP</th>
                                        <th class="head1 center">NAMA</th>
                                        <th class="head0 center">UMUR</th>
                                        <th class="head0 center">ALAMAT</th>
                                        <th class="head1 center">UNIT KERJA</th>
                                        <th class="head1 center">JABATAN</th>
                                        <th class="head0 center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($dt_pegawai)){foreach ($dt_pegawai as $row2){
                                        if($row->id_unit == $row2->id_unit) { ?>
                                        <tr>
                                            <td class="center"><input type="checkbox" name="cek[]" value="<?php echo $row2->id_pegawai?>"/></td>
                                            <td><?php echo $row2->NIP?></td>
                                            <td><?php echo $row2->nm_pegawai?></td>
                                            <td><?php echo $row2->umur?></td>
                                            <td><?php echo $row2->alamat?></td>
                                            <td class="text-uc"><?php echo $row2->unit_kerja?></td>
                                            <td><?php echo $row2->nm_posisi?></td>
                                            <td class="center">
                                                <a href="<?= site_url('master/view_pegawai/'.$row2->id_pegawai)?>" class="btn btn-mini">
                                                    <i class="icon-edit"></i>View</a>
                                                <a href="<?= site_url('master/edit_pegawai/'.$row2->id_pegawai)?>" class="btn btn-mini">
                                                    <i class="icon-edit"></i> Edit</a>
                                                <a href="<?= site_url('master/skor_pegawai/'.$row2->unit_kerja.'/'.$row2->id_pegawai)?>" class="btn btn-mini">
                                                    <i class="icon-edit"></i> Setting Skor</a>
                                            </td>
                                        </tr>
                                    <?php } } } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } } ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!--print-->
<div class="visible-print">
    <div class="container">
        <div class="pagetitle">
            <h1 style="text-align: center">SISTEM PENDUKUNG KEPUTUSAN PENENTUAN POSISI JABATAN IDEAL PEGAWAI KEPENDIDIKAN DAN ADMINISTRASI DENGAN ALGORITMA GENETIKA BERBASIS WEB</h1>
            <h3 style="text-align: center">(Studi Kasus : Sekolah Tinggi Teknologi Adisutjipto)</h3>

        </div>

        <!--<address>
            <strong>Tanggal: </strong>12/09/2014
            <br/>
            <strong>Kategori:</strong> <span class="text-upper">Kependidikan</span>
            <br/>
        </address>-->
        <div id="tabs-1" aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">
            <table class="table">
                <colgroup>
                    <col class="con1 width4" />
                    <col class="con0 width5"/>
                    <col class="con1 width10"/>
                    <col class="con0 width4"/>
                    <col class="con0 width10"/>
                    <col class="con1 width4"/>
                    <col class="con0 width4"/>

                </colgroup>
                <thead>
                <tr>
                    <th class="head1 center"><input type="checkbox" name="checkall" class="checkall" /></th>
                    <th class="head0 center">NIP</th>
                    <th class="head1 center">NAMA</th>
                    <th class="head0 center">UMUR</th>
                    <th class="head0 center">ALAMAT</th>
                    <th class="head1 center">UNIT KERJA</th>
                    <th class="head1 center">JABATAN</th>

                </tr>
                </thead>
                <tbody>
                <?php if(isset($dt_pegawai)){foreach($dt_pegawai as $row){?>
                    <tr>
                        <td class="center"><input type="checkbox" name="cek[]" value="<?php echo $row->id_pegawai?>"/></td>
                        <td><?php echo $row->NIP?></td>
                        <td><?php echo $row->nm_pegawai?></td>
                        <td><?php echo $row->umur?></td>
                        <td><?php echo $row->alamat?></td>
                        <td class="text-uc"><?php echo $row->unit_kerja?></td>
                        <td><?php echo $row->nm_posisi?></td>

                    </tr>
                <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>