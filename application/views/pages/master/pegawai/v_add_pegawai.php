<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar')?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard')?>">Dashboard</a> <span class="divider">/</span></li>
            <li><a href="<?php echo site_url('backend/website/room')?>">Pegawai</a> <span class="divider">/</span></li>
            <li class="active">Tambah Pegawai</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Add Pegawai</h1> <span>Tambah Pegawai</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <h4 class="widgettitle nomargin shadowed">Input Form</h4>
            <div class="widgetcontent bordered shadowed ">
                <form id="frm-posting" class="stdform" method="post" action="<?php echo site_url('master/input_pegawai')?>">
                    <div class="control-group">
                        <div class="controls">
                            <input type="hidden" name="id_pegawai" class="input-xlarge" value="<?= $id_pegawai?>" readonly/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">NIP</label>
                        <div class="controls">
                            <input type="text" name="NIP" class="input-xlarge" value="" required=""/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Pegawai</label>
                        <div class="controls">
                            <input type="text" name="nm_pegawai" class="input-xlarge" value="" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Umur</label>
                        <div class="controls">
                            <input type="text" name="umur" class="input-xlarge" value="" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            <input type="text" name="alamat" class="input-xlarge" value="" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Posisi Jabatan</label>
                        <div class="controls">
                            <select id="selectPosisi" name="id_posisi">
                                <option value="">Pilih Posisi Jabatan</option>
                                <?php if(isset($dt_posisi)) { foreach ($dt_posisi as $row) { ?>
                                    <option value="<?= $row->id_posisi ?>"><?= $row->nm_posisi?></option>
                                <?php } } ?>
                            </select>
                        </div>
                    </div>
                    <hr/>
                    <p class="stdformbutton">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?= site_url('master/pegawai') ?>" class="btn"> Back</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
