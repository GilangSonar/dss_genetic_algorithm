<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar')?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard')?>">Dashboard</a> <span class="divider">/</span></li>
            <li><a href="<?php echo site_url('backend/website/room')?>">Pegawai</a> <span class="divider">/</span></li>
            <li class="active">Edit Pegawai</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Pegawai</h1> <span>Edit Pegawai</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <h4 class="widgettitle nomargin shadowed">Edit Form</h4>
            <div class="widgetcontent bordered shadowed ">
                <?php  if(isset($dt_pegawai)){foreach ($dt_pegawai as $rowData){ ?>
                <form id="frm-posting" class="stdform" method="post" action="<?php echo site_url('master/update_pegawai')?>">
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" name="id_pegawai" class="input-xlarge" value="<?= $rowData->id_pegawai?>" required="" style="display: none">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">NIP</label>
                        <div class="controls">
                            <input type="text" name="NIP" class="input-xlarge" value="<?= $rowData->NIP?>" required=""/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama Pegawai</label>
                        <div class="controls">
                            <input type="text" name="nm_pegawai" class="input-xlarge" value="<?= $rowData->nm_pegawai?>" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Umur</label>
                        <div class="controls">
                            <input type="text" name="umur" class="input-xlarge" value="<?= $rowData->umur?>" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            <input type="text" name="alamat" class="input-xlarge" value="<?= $rowData->alamat?>" required="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Posisi Jabatan</label>
                        <div class="controls">
                            <select name="id_posisi">
                                <?php if(isset($dt_posisi)){foreach ($dt_posisi as $row){
                                    if($rowData->id_posisi == $row->id_posisi){
                                        $selected = "selected=selected";
                                    }else{
                                        $selected = "";
                                    }?>
                                    <option <?php echo $selected; ?> value="<?php echo $row->id_posisi ?>">
                                        <?php echo $row->nm_posisi ?>
                                    </option>
                                <?php } }?>
                            </select>
                        </div>
                    </div>
                    <p class="stdformbutton">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="<?= site_url('master/pegawai') ?>" class="btn"> Back</a>
                    </p>
                </form>
            </div>
            <?php }  } ?>
        </div>
    </div>

</div>


