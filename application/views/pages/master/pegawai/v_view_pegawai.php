<div class="hidden-print">
    <?php $this->load->view('subelement/v_sidebar') ?>
    <div class="rightpanel">
        <?php $this->load->view('subelement/v_topbar') ?>
        <div class="breadcrumbwidget">
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
                <li class="active">Master Data</li>
                <span class="divider">/</span>
                <li class="active">Pegawai</li>
                <span class="divider">/</span>
                <li class="active">View Pegawai</li>
            </ul>
        </div>
        <div class="pagetitle">
            <h1>View Pegawai</h1>
        </div>
        <div class="maincontent">
            <div class="contentinner">
                <h4 class="widgettitle nomargin shadowed">View Pegawai</h4>
                <div class="widgetcontent bordered shadowed ">
                    <?php  if(isset($dt_pegawai)){foreach ($dt_pegawai as $rowData){ ?>
                        <div class="row-fluid">
                            <div class="span4 text-uc">
                                <?php if(isset($dt_pegawai)){foreach($dt_pegawai as $row) { ?>
                                    <input type="hidden" name="id_pegawai" value="<?= $row->id_pegawai?>" readonly/>
                                    <dl class="dl-horizontal">
                                        <dt>NIP: </dt> <dd><?= $row->NIP?></dd>
                                        <dt>Nama Pegawai: </dt> <dd><?= $row->nm_pegawai?></dd>
                                        <dt>Umur: </dt> <dd><?= $row->umur?></dd>
                                        <dt>Alamat: </dt> <dd><?= $row->umur?></dd>
                                        <br/>
                                        <dt>Jabatan Saat Ini: </dt> <dd><?= $row->nm_posisi?></dd>
                                        <dt>Unit Kerja: </dt> <dd><?= $row->unit_kerja?></dd>
                                    </dl>
                                    <hr/>
                                    <div class="alert alert-info">
                                        <h3>KETERANGAN SKOR ( 1 - 5 )</h3>
                                        <strong>Skor 5 : SANGAT BAIK</strong><br/>
                                        <strong>Skor 4 : BAIK</strong><br/>
                                        <strong>Skor 3 : CUKUP</strong><br/>
                                        <strong>Skor 2 : KURANG</strong><br/>
                                        <strong>Skor 1 : SANGAT KURANG</strong>
                                    </div>
                                <?php } } ?>
                            </div>
                            <div class="span8">
                                <table class="table table-bordered mailinbox">
                                    <colgroup>
                                        <col class="con0"/>
                                        <col class="con1"/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th class="head0 center">Kriteria</th>
                                        <th class="head1 center span2">Skor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($dt_kriteria)){ foreach($dt_kriteria as $row) {
                                        if($row->id_unit == $rowData->id_unit){
                                            $param['id_kriteria']= $row->id_kriteria;
                                            $param['id_pegawai']= $rowData->id_pegawai;
                                            $skor = $this->m_custom->getSelectData('tbl_skor_pegawai',$param)->result();
                                            ?>
                                        <tr>
                                            <td><?= $row->nm_kriteria?></td>
                                            <td>
                                                <?php if(!empty($skor[0]->skor)) {
                                                    echo $skor[0]->skor;
                                                }else{
                                                    echo "<i>NULL</i>";
                                                }?>
                                            </td>
                                        </tr>
                                    <?php } } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <p class="text-center">
                                <a href="" class="btn btn-danger" onclick="window.print()">
                                    <i class="icon icon-white icon-print"></i> Print</a>
                                <a href="<?= site_url('master/edit_pegawai/'.$rowData->id_pegawai)?>" class="btn btn-primary">
                                    <i class="icon icon-white icon-edit"></i> Edit</a>
                                <a href="<?= site_url('master/skor_pegawai/'.$rowData->unit_kerja.'/'.$rowData->id_pegawai)?>" class="btn btn-primary">
                                    <i class="icon icon-white icon-edit"></i> Set Skor</a>
                            </p>
                    <?php }  } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!----------------- PRINT ----------------->
<div class="visible-print">
    <div class="container">
        <div class="pagetitle">
            <h1 style="text-align: center">SISTEM PENDUKUNG KEPUTUSAN PENENTUAN POSISI JABATAN IDEAL PEGAWAI KEPENDIDIKAN DAN ADMINISTRASI DENGAN ALGORITMA GENETIKA BERBASIS WEB</h1>
            <h3 style="text-align: center">(Studi Kasus : Sekolah Tinggi Teknologi Adisutjipto)</h3>

        </div>
       <?php  if(isset($dt_pegawai)){foreach ($dt_pegawai as $row){ ?>
            <div class="row-fluid">
                <div class="span2 text-uc ">
                    <?php if(isset($dt_pegawai)){foreach($dt_pegawai as $row) { ?>
                        <input type="hidden" name="id_pegawai" value="<?= $row->id_pegawai?>" readonly/>
                        <dl class="dl-horizontal">
                            <dt>Tanggal: </dt><dd>12/09/89</dd>
                            <dt>NIP: </dt> <dd><?= $row->NIP?></dd>
                            <dt>Nama Pegawai: </dt> <dd><?= $row->nm_pegawai?></dd>
                            <dt>Umur: </dt> <dd><?= $row->umur?></dd>
                            <dt>Alamat: </dt> <dd><?= $row->umur?></dd>
                            <dt>Jabatan Saat Ini: </dt> <dd><?= $row->nm_posisi?></dd>
                            <dt>Unit Kerja: </dt> <dd><?= $row->unit_kerja?></dd>
                        </dl>
                    <?php } } ?>
                </div>
                <div class="span8">
                    <table class="table table-bordered mailinbox">
                        <colgroup>
                            <col class="con0"/>
                            <col class="con1"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="head0 center">Kriteria</th>
                            <th class="head1 center span1">Skor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($dt_kriteria)){ foreach($dt_kriteria as $row) {?>
                            <tr>
                                <td>
                                    <input type="hidden" name="kriteria[]" value="<?= $row->id_kriteria?>"/>
                                    <?= $row->nm_kriteria?>
                                </td>
                                <td>
                                    <input name="skor[]" class="form-control text-center " type="number" max="5" min="0" value="<?php if(isset($row->skor)){ echo $row->skor;}else{ echo " ";}?>" required=""/>
                                </td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }  } ?>
    </div>
        <!-- END PRINT-->



