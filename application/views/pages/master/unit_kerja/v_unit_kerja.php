<?php $this->load->view('subelement/v_sidebar') ?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar') ?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard') ?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Master Data</li>
            <span class="divider">/</span>
            <li class="active">Unit Kerja</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Unit Kerja</h1> <span>Daftar unit kerja</span>
        <a href="#modalAdd" data-toggle="modal" class="pull-right btn-group btn btn-primary"><i class="icon-plus-sign icon-white"></i> Tambah Data</a>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <form method="post" action="<?= site_url('master/delete_unit_kerja')?>">
                <div class="msghead">
                    <ul class="msghead_menu">
                        <li class="right">
                            <button type="submit" class="btn" onclick="return confirm('Are you sure want to delete ?');">
                                <span class="iconsweets-trashcan"></span> Hapus Data
                            </button>
                        </li>
                    </ul>
                    <span class="clearall"></span>
                </div>
                <br>
                <table class="table table-bordered mailinbox dyntable">
                    <colgroup>
                        <col class="con1 width4" />
                        <col class="con0 width10"/>
                        <col class="con0 width10"/>
                        <col class="con0 width4"/>
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head1 center"><input type="checkbox" name="checkall" class="checkall" /></th>
                        <th class="head0 center">Kode Unit Kerja</th>
                        <th class="head0 center">Unit Kerja</th>
                        <th class="head0 center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $row){ ?>
                        <tr>
                            <td class="center">
                                <input type="checkbox" name="cek[]" value="<?php echo $row->id_unit?>"/>
                            </td>
                            <td class="text-uc"><?php echo $row->id_unit?></td>
                            <td class="text-uc"><?php echo $row->unit_kerja?></td>
                            <td class="center">
                                <a href="#modalEdit<?=$row->id_unit?>" class="btn btn-mini btn-block" data-toggle="modal">
                                    <i class="icon-edit"></i> Edit</a>
                            </td>
                        </tr>
                    <?php } } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!--======== MODAL ADD =================  -->
<form class="form" method="post" action="<?php echo site_url('master/input_unit_kerja')?>">
    <div aria-hidden="false" aria-labelledby="myModalLabel" class="modal hide fade in" id="modalAdd">
        <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 id="myModalLabel">Tambah Data</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label" for="id_unit">Kode Unit Kerja</label>
                <div class="controls">
                    <input type="text" name="id_unit" class="input-xlarge" value="<?= $id_unit?>" readonly />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Unit Kerja</label>
                <div class="controls">
                    <input type="text" name="unit_kerja" class="input-xlarge"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn">Close</button>
            <button class="btn btn-primary">Save changes</button>
        </div>
    </div>
</form>

<!--=================== MODAL EDIT ========================-->
<?php  if(isset($dt_unit_kerja)){foreach ($dt_unit_kerja as $row){ ?>
    <form class="form" method="post" action="<?php echo site_url('master/edit_unit_kerja')?>">
        <div aria-hidden="false" aria-labelledby="myModalLabel" class="modal hide fade in" id="modalEdit<?=$row->id_unit?>">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h3 id="myModalLabel">Edit Data</h3>
            </div>
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label">Kode Unit Kerja</label>
                    <div class="controls">
                        <input type="text" name="id_unit" class="input-xlarge" value="<?= $row->id_unit ?>" readonly />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Unit Kerja</label>
                    <div class="controls">
                        <input type="text" name="unit_kerja" class="input-xlarge" value="<?= $row->unit_kerja?>" required=""/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </form>
<?php }  } ?>








