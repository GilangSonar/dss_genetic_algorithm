<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar')?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li class="active">Dashboard</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Dashboard</h1> <span>Overview Data</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <div class="row-fluid">
                <div class="span5">&nbsp;</div>
                <div class="span2">
                    <div class="thumbnail-borderless">
                        <img src="<?= base_url('asset/img/stta.png') ?>" alt="Logo STTA"/>
                    </div>
                </div>
                <div class="span5">&nbsp;</div>
            </div>
            <div class="clearfix"></div>
            <hr/>

            <div class="row-fluid">
                <h2 class="text-center">
                    SISTEM PENDUKUNG KEPUTUSAN PENENTUAN POSISI JABATAN IDEAL PEGAWAI KEPENDIDIKAN DAN ADMINISTRASI DENGAN ALGORITMA GENETIKA BERBASIS WEB
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

