<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar')?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li class="active">Proses</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Seleksi <?= $dt_posisi[0]->unit_kerja?></h1> <span>Seleksi Posisi Jabatan Pegawai <?= $dt_posisi[0]->unit_kerja?></span>
    </div>

    <div class="maincontent">
        <div class="contentinner">

            <div class="row-fluid">
                <?php if(isset($dt_posisi)){foreach($dt_posisi as $r){?>
                    <div class="span8">
                        <label class="control-label text-uc">Pilih Pegawai Yang Ingin Diseleksi &nbsp;&nbsp; POSISI : <?php echo $r->nm_posisi?></label>
                        <table class="table table-bordered mailinbox dyntable">
                            <colgroup>
                                <col class="con0 width5"/>
                                <col class="con1 width10"/>
                                <col class="con1 width4"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th class="head0 center">NIP</th>
                                <th class="head1 center">NAMA</th>
                                <th class="head1 center">OPTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($dt_pegawai)){foreach($dt_pegawai as $row){?>
                                <tr>
                                    <td><?php echo $row->NIP?></td>
                                    <td><?php echo $row->nm_pegawai?></td>
                                    <td>
                                        <a href="<?= site_url('proses/temp/'.$r->id_unit.'/'.$row->id_pegawai.'/'.$r->id_posisi)?>" class="btn btn-mini btn-primary btn-block">
                                            <i class="icon icon-white icon-chevron-right"></i> PILIH
                                        </a>
                                    </td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } } ?>

                <div class="span4">
                    <label class="control-label text-uc">Daftar pegawai yang dipilih</label>
                    <table class="table table-bordered mailinbox">
                        <colgroup>
                            <col class="con0 width5"/>
                            <col class="con1 width10"/>
                            <col class="con1 width4"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="head0 center">NIP</th>
                            <th class="head1 center">NAMA</th>
                            <th class="head1 center">OPTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($dt_temp_seleksi)){foreach($dt_temp_seleksi as $row){?>
                            <tr>
                                <td><?php echo $row->NIP?></td>
                                <td><?php echo $row->nm_pegawai?></td>
                                <td>
                                    <a href="<?= site_url('proses/deleteTemp/'.$dt_posisi[0]->id_unit.'/'.$row->id_posisi.'/'.$row->id_pegawai)?>" class="btn btn-mini btn-danger btn-block">
                                        <i class="icon icon-white icon-trash"></i> Hapus
                                    </a>
                                </td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <hr/>
            <form method="post" action="<?= site_url('proses_ga')?>">

                <?php if(isset($param)) { foreach ($param as $row) { ?>
                    <input type="hidden" id="generation" name="generation" value="<?= $row->generasi?>" />
                    <input type="hidden" id="population" name="population" value="<?= $row->populasi?>" />
                    <input type="hidden" id="crossover" name="crossover" value="<?= $row->kawin_silang/100 ?>" />
                    <input type="hidden" id="mutate" name="mutate" value="<?= $row->mutasi/100 ?>" />
                    <input type="hidden" id="elistism" name="elistism" value="<?= $row->elistism/100 ?>" />
                <?php } }?>

                <button id="submitBtn" class="btn btn-primary"> Proses Data </button>
                <a href="<?= site_url('proses/seleksi/'.$dt_posisi[0]->id_unit)?>" class="btn"> <i class="icon icon-remove-circle"></i> Cancel </a>
            </form>
        </div>
    </div>

</div>
<div class="clearfix"></div>