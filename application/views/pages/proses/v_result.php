<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
<?php $this->load->view('subelement/v_topbar')?>
<div class="breadcrumbwidget">
    <ul class="breadcrumb">
        <li class="active">Hasil Proses</li>
    </ul>
</div>
<div class="pagetitle">
    <h1>Hasil Proses</h1> <span>Hasil Proses SPK Dengan Metode Algoritma Genetika</span>
</div>
<div class="maincontent">
<div class="contentinner">
<h4 class="widgettitle nomargin shadowed">Detail Proses</h4>
<div class="widgetcontent bordered shadowed ">
<dl class="dl-horizontal">
    <dt> DAFTAR PEGAWAI : </dt>
    <dd>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="center">ID Pegawai</th>
                <th class="center">Nama Pegawai</th>
                <th class="center">Skor</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($dt_kandidat)) {  foreach ($dt_kandidat as $k) { ?>
                <tr>
                    <td>Ke- <?= $k->id_pegawai?></td>
                    <td><?= $k->nm_pegawai?></td>
                    <td><?= $k->total?></td>
                </tr>
            <?php } } ?>
            </tbody>
        </table>
    </dd>
</dl>

<hr/>
<?php if(!empty($dt_gene)) {
    $winner = array();
    foreach ($dt_gene as $k => $row) {
        ?>
        <div class="alert alert-info text-center">
            <h3>GENERASI KE - <?= $k+1?></h3>
        </div>

        <dl class="dl-horizontal">

            <?php if($k != 0 ) { ?>
                <dt> POPULASI AWAL: </dt>
                <dd>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="center span1">Populasi</th>
                            <th class="center">Chromosome</th>
                            <th class="center">Fitness</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($dt_gene)) {
                            foreach ($dt_gene[$k-1] as $key=>$res) :
                                ?>
                                <tr>
                                    <td><?= $res->no_pop?> </td>
                                    <td><?= $res->chromosome?></td>
                                    <td><?= $res->fitness?></td>
                                </tr>
                            <?php endforeach; }
                        ?>
                        </tbody>
                    </table>
                </dd>
                <br/>
            <?php } ?>
            <dt>CROSSOVER (PC : <?= $paramC; ?>):</dt>
            <dd><u>Random angka untuk pemilihan chromosome</u> </dd>
            <dd>
                <?php
                if(isset($randC)) {
                    foreach ($randC as $rc) {
                        if($rc['no_gene']-1 == $k) { ?>
                            <span class="<?php if($rc['number'] <= $paramC){echo "text-info";}?>"><?= $rc['number']?></span> |
                            <?php
                        }
                    }
                } ?>
            </dd>

            <dd><u>Chromosome terpilih</u></dd>
            <dd>
                <?php
                if(isset($parentC)) {
                    foreach ($parentC as $pc) {
                        if($pc['generation']-1 == $k) { ?>
                            <?= $pc['chromosome']?> |
                            <?php
                        }
                    }
                } ?>
            </dd>
            <br/>

            <dt>MUTATE (PM : <?= $paramM; ?>): </dt>
            <dd><u>Random angka untuk pemilihan chromosome</u> </dd>
            <dd>
                <?php
                if(isset($randM)) {
                    foreach ($randM as $rm) {
                        if($rm['no_gene']-1 == $k) { ?>
                            <span class="<?php if($rm['number'] <= $paramM){echo "text-info";}?>"><?= $rm['number']?></span> |
                            <?php
                        }
                    }
                } ?>
            </dd>

            <dd><u>Chromosome terpilih</u></dd>
            <dd>
                <?php
                if(isset($parentM)) {
                    foreach ($parentM as $pm) {
                        if($pm['generation']-1 == $k) {?>
                            <?= $pm['chromosome']?> |
                            <?php
                        }
                    }
                } ?>
            </dd>

            <br/>
            <dt>ELISTISM (PE : <?= $paramE; ?>): </dt>
            <dd><u>Random angka untuk pemilihan chromosome</u> </dd>
            <dd>
                <?php
                if(isset($randE)) {
                    foreach ($randE as $re) {
                        if($re['no_gene']-1 == $k) { ?>
                            <span class="<?php if($re['number'] <= $paramE){echo "text-info";}?>"><?= $re['number']?></span> |
                            <?php
                        }
                    }
                } ?>
            </dd>

            <dd><u>Chromosome terpilih</u></dd>
            <dd>
                <?php
                if(isset($parentE)) {
                    foreach ($parentE as $pe) {
                        if($pe['generation']-1 == $k) { ?>
                            <?= $pe['chromosome']?> |
                            <?php
                        }
                    }
                } ?>
            </dd>

            <br/>

            <dt> HASIL GENERASI KE - <?= $k+1?>: </dt>
            <dd>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="center span1">Populasi</th>
                        <th class="center">Chromosome</th>
                        <th class="center">Fitness</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($dt_gene)) {
                        $maxfitness = array();
                        foreach ($dt_gene[$k] as $key=>$res) {
                            $maxfitness[$key] = $res->fitness;
                            ?>
                            <tr>
                                <td><?= $res->no_pop?> </td>
                                <td><?= $res->chromosome?></td>
                                <td><?= $res->fitness?></td>
                            </tr>
                        <?php }
                        $selectedKandidat = max($maxfitness);
                        // find chromosome with the highest fitness
                        $select_key = array_search($selectedKandidat, $maxfitness);
                        $chromosome_with_highest_fitness = $dt_gene[$k][$select_key]->chromosome;
                        // explode chromosome and put the first
                        $explode_chromosome = explode(',',$chromosome_with_highest_fitness);
                        $first_chromosome = $explode_chromosome[0];
                    } ?>
                    </tbody>
                </table>
            </dd>
            <br/>
            <dt>FITNESS TERBESAR : </dt>
            <?php
            if(!empty($dt_kandidat)) {
                foreach ($dt_kandidat as $kan) {
                    if($kan->total == $first_chromosome ) :
                        $winner[$k] = $kan->nm_pegawai;
                        ?>
                        <dd>
                            <?= $selectedKandidat?>
                        </dd>
                    <?php
                    endif;
                }
            }
            ?>
            <br/>
            <dt>PEGAWAI TERPILIH : </dt>
            <?php
            if(!empty($dt_kandidat)) {
                foreach ($dt_kandidat as $kan) {
                    if($kan->total == $first_chromosome ) :
                        $winner[$k] = $kan->nm_pegawai;
                        ?>
                        <dd class="text-uc text-info">
                            <span class="label label-important"><strong><?= $kan->nm_pegawai?> || Skor : <?= $kan->total; ?></strong></span>
                        </dd>
                    <?php
                    endif;
                }
            }
            ?>
        </dl>

    <?php }
    $values = array_count_values($winner);
    $pegawai = array_search(max($values),$values);
    ?>
    <hr/>
    <div class="alert alert-success text-center">
        <h3 class="text-uc">
            <strong>PEGAWAI YANG TERPILIH ADALAH : <u><?= $pegawai; ?></u></strong>
        </h3>
    </div>
<?php }
?>

</div>
</div>
</div>
</div>
<div class="clearfix"></div>


