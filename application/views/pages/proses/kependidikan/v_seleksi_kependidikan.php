<?php $this->load->view('subelement/v_sidebar')?>
<div class="rightpanel">
    <?php $this->load->view('subelement/v_topbar')?>
    <div class="breadcrumbwidget">
        <ul class="breadcrumb">
            <li class="active">Proses</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Seleksi Kependidikan</h1> <span>Seleksi Posisi Jabatan Pegawai Kependidikan</span>
    </div>
    <div class="maincontent">
        <div class="contentinner">
            <ul class="widgeticons row-fluid">
                <?php if(isset($dt_posisi)) { foreach($dt_posisi as $row) {?>
                    <li class="one_half"><a href="<?= site_url('proses/seleksi_kependidikan/'.$row->id_posisi)?>"><img src="<?= base_url('asset/img/users.png')?>" alt="" class="animate0 bounceIn"><span>Seleksi <?= $row->nm_posisi?></span></a></li>
                <?php } } ?>
            </ul>
        </div>
    </div>
</div>
<div class="clearfix"></div>


