<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Katniss Premium Admin Template</title>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/style.default.css')?>" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-1.9.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-migrate-1.1.1.min.js')?>"></script>
</head>

<body class="loginbody">

<div class="loginwrapper">
    <div class="loginwrap zindex100 animate2 bounceInDown">
        <h1 class="logintitle"><span class="iconfa-lock"></span> Sign In <span class="subtitle">Hello! Sign in to get you started!</span></h1>
        <div class="loginwrapperinner">
            <form id="loginform" action="<?php echo base_url()?>panel/login" method="post">
                <?php
                $alert = $this->session->flashdata('alert');
                if($alert){?>
                    <h4 class="text-center" style="color: #ffffff;"><?= $alert?></h4>
                <?php } ?>

                <p class="animate4 bounceIn">
                    <input type="text" id="username" name="username" placeholder="Username..." />
                </p>
                <p class="animate5 bounceIn">
                    <input type="password" id="password" name="password" placeholder="Password..." />
                </p>
                <p class="animate6 bounceIn">
                    <button class="btn btn-default btn-block" type="submit">Submit</button>
                </p>

            </form>
        </div><!--loginwrapperinner-->
    </div>
    <div class="loginshadow animate3 fadeInUp"></div>
</div><!--loginwrapper-->

</body>
</html>
