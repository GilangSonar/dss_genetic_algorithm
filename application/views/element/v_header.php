<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $title ?></title>
    <!--    STYLE-->
    <link rel="stylesheet" href="<?php echo base_url('asset/css/style.default.css')?>" type="text/css" />
    <!--    SCRIPT JS-->
    <script type="text/javascript" src="<?php echo base_url('asset/js/modernizr.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/detectizr.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/prettify/prettify.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-1.9.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-migrate-1.1.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-ui-1.9.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-fileupload.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.uniform.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/chosen.jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.validate.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.gritter.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/tinymce/tinymce.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/custom.min.js')?>"></script>
</head>
<body>
