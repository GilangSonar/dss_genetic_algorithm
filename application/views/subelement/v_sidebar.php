<div class="leftpanel">
    <div class="logopanel">
        <h1><a href="#">SKRIPSI STTA ( TF )</a></h1>
    </div>
    <div class="datewidget"><strong>Sistem Pendukung Keputusan</strong></div>
    <div class="leftmenu">
        <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header" style="height: 49px"><h1>MENU</h1></li>
            <li class="<?php if (isset($act_dashboard)){echo $act_dashboard;} ?>"> <a href=<?php echo site_url('dashboard')?>><span class="icon-align-justify"></span> Dashboard</a></li>
            <li class="<?php if (isset($act_proses)){echo $act_proses;}?> dropdown "> <a href="#"><span class="icon-th-list"></span> Proses</a>
                <ul style="<?php if(isset($display_proses)){echo $display_proses;} ?>">
                    <?php $unit = $this->m_custom->getAllData('tbl_unit_kerja');
                    foreach($unit as $row ){?>
                        <li style="<?php if($row->id_unit == $this->uri->segment(3)){echo "background:#88c4e2";}?>"><a href="<?= site_url('proses/seleksi/'.$row->id_unit) ?>">Seleksi <?= $row->unit_kerja?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li class="dropdown <?php if (isset($act_setting)){echo $act_setting;}?>"><a href="#"><span class="icon-book"></span>Setting </a>
                <ul style="<?php if(isset($display_setting)){echo $display_setting;} ?>">
                    <li style="<?php if (isset($active_parameter)){echo $active_parameter;}?>"><a href="<?= site_url('setting/parameter') ?>">Parameter</a></li>
                    <li style="<?php if (isset($active_bobot)){echo $active_bobot;}?>"><a href="<?= site_url('setting/bobot') ?>">Bobot Posisi Jabatan</a></li>
                </ul>
            </li>

            <li class="dropdown <?php if (isset($act_master)){echo $act_master;}?>"><a href="#"><span class="icon-file"></span>Master Data </a>
                <ul style="<?php if(isset($display_master)){echo $display_master;} ?>">
                    <li style="<?php if (isset($active_unit)){echo $active_unit;}?>"><a href="<?= site_url('master/unit_kerja') ?>">Unit Kerja</a></li>
                    <li style="<?php if (isset($active_pegawai)){echo $active_pegawai;}?>"><a href="<?= site_url('master/pegawai') ?>">Pegawai</a></li>
                    <li style="<?php if (isset($active_posisi)){echo $active_posisi;}?>"><a href="<?= site_url('master/posisi') ?>">Posisi Jabatan</a></li>
                    <li style="<?php if (isset($active_kriteria)){echo $active_kriteria;}?>"><a href="<?= site_url('master/kriteria') ?>">Kriteria Pemilihan</a></li>
                    <li style="<?php if (isset($active_member)){echo $active_member;}?>"><a href="<?= site_url('master/member') ?>">Member</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>