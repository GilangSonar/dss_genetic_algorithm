<?php
class M_custom extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function getAllData($table){
        $query=$this->db->get($table);
        return $query->result();
    }
    function getSelectData($table,$id){
        return $this->db->get_where($table,$id);
    }
    function insertData($table,$data){
        $this->db->insert($table,$data);
    }
    function updateData($table,$data,$id){
        $this->db->update($table,$data,$id);
    }
    function deleteData($table,$id){
        $this->db->delete($table,$id);
    }
    function manualQuery($q){
        return $this->db->query($q)->result();
    }
    function manual($q){
        return $this->db->query($q);
    }
    function truncate($table){
        $this->db->truncate($table);
    }
// ======================================
// MULTIPLE DELETE WITH CHECKLIST
// =======================================

    function deleteMemberList($id){
        $this->db->where_in('id_member',$id);
        $this->db->delete('tbl_member');
        return $this->db->affected_rows() > 0;
    }
    function deleteKriteria($id){
        $this->db->where_in('id_kriteria',$id);
        $this->db->delete('tbl_kriteria');
        return $this->db->affected_rows() > 0;
    }
    function deleteKriteriaKependidikanList($id){
        $this->db->where_in('id_kriteria_kependidikan',$id);
        $this->db->delete('tbl_kriteria_kependidikan');
        return $this->db->affected_rows() > 0;
    }
    function deleteKriteriaAdministrasiList($id){
        $this->db->where_in('id_kriteria_administrasi',$id);
        $this->db->delete('tbl_kriteria_administrasi');
        return $this->db->affected_rows() > 0;
    }
    function deletePosisiList($id){
        $this->db->where_in('id_posisi',$id);
        $this->db->delete('tbl_posisi');
        return $this->db->affected_rows() > 0;
    }
    function deletePegawaiList($id){
        $this->db->where_in('id_pegawai',$id);
        $this->db->delete('tbl_pegawai');
        return $this->db->affected_rows() > 0;
    }
    function deleteUnitKerja($id){
        $this->db->where_in('id_unit',$id);
        $this->db->delete('tbl_unit_kerja');
        return $this->db->affected_rows() > 0;
    }

// ==============================
//          GET KODE
// ==============================

    function getKodeMember(){
        $q = $this->db->query("select MAX(RIGHT(id_member,3)) as code_max from tbl_member");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "M-".$code;
    }
    function getKodeKriteria(){
        $q = $this->db->query("select MAX(RIGHT(id_kriteria,3)) as code_max from tbl_kriteria");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "K-".$code;
    }
    function getKodeKriteriaKependidikan(){
        $q = $this->db->query("select MAX(RIGHT(id_kriteria_kependidikan,3)) as code_max from tbl_kriteria_kependidikan");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "KK".$code;
    }
    function getKodeKriteriaAdministrasi(){
        $q = $this->db->query("select MAX(RIGHT(id_kriteria_administrasi,3)) as code_max from tbl_kriteria_administrasi");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return "KA".$code;
    }
    function getKodePosisi(){
        $q = $this->db->query("select MAX(RIGHT(id_posisi,2)) as code_max from tbl_posisi");
        $code = "";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%02s", $tmp);
            }
        }else{
            $code = "01";
        }
        return "PJ-".$code;
    }
    function getKodePegawai(){
        $q = $this->db->query("select MAX(RIGHT(id_pegawai,3)) as code_max from tbl_pegawai");
        $code="";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
             $tmp = ((int)$cd->code_max)+1;
             $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return"P-".$code;
    }

    function getKodeCross(){
        $q = $this->db->query("select MAX(RIGHT(id_cross,3)) as code_max from _temp_cross");
        $code="";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return"C-".$code;
    }

    function getKodeUnit(){
        $q = $this->db->query("select MAX(RIGHT(id_unit,3)) as code_max from tbl_unit_kerja");
        $code="";
        if($q->num_rows()>0){
            foreach($q->result() as $cd){
                $tmp = ((int)$cd->code_max)+1;
                $code = sprintf("%03s", $tmp);
            }
        }else{
            $code = "001";
        }
        return"UK-".$code;
    }
// ========================
//      OTHER QUERY
// ========================
    function getSeleksiPegawai($id){
        $this->db->select('*');
        $this->db->from('view_pegawai');
        $this->db->where($id);
        $this->db->where('status','1');
        $sql = $this->db->get();
        return $sql->result();
    }

    function getSkorPegawai($id){
        $this->db->select('a.id_pegawai,b.id_kriteria,b.nm_kriteria,a.skor');
        $this->db->from('tbl_skor_pegawai AS a');
        $this->db->join('tbl_kriteria AS b','a.id_kriteria=b.id_kriteria','left');
        $this->db->where('a.id_pegawai',$id);
        $this->db->order_by('b.id_kriteria','asc');
        $sql = $this->db->get();
        return $sql->result();
    }
    function getBobotPosisi($id){
        $this->db->select('a.id_posisi,b.id_kriteria,b.nm_kriteria,a.bobot');
        $this->db->from('tbl_bobot_posisi AS a');
        $this->db->join('tbl_kriteria AS b','a.id_kriteria=b.id_kriteria','left');
        $this->db->where('a.id_posisi',$id);
        $this->db->order_by('b.id_kriteria','asc');
        $sql = $this->db->get();
        return $sql->result();
    }
    function getTempSeleksi($id){
        $this->db->select('a.*,b.nm_pegawai,b.NIP');
        $this->db->from('_temp_seleksi AS a');
        $this->db->join('tbl_pegawai AS b','a.id_pegawai=b.id_pegawai','left');
        $this->db->where('a.id_posisi',$id);
        $sql = $this->db->get();
        return $sql->result();
    }
    function getAllTempSeleksi(){
        $this->db->select('a.*,b.nm_pegawai,b.NIP');
        $this->db->from('_temp_seleksi AS a');
        $this->db->join('tbl_pegawai AS b','a.id_pegawai=b.id_pegawai','left');
        $sql = $this->db->get();
        return $sql->result();
    }
    function getPegawaiNoSelect($id){
        return $this->db->query("select * from view_pegawai
              where id_unit = '$id' and
        id_pegawai NOT IN (
            select id_pegawai from _temp_seleksi
              )")->result();
    }


    // FOR Genetic Algorithm
    function selectCross(){
        return $this->db->query("select * from _temp_cross limit 1")->result();
    }
    function getResultCross1(){
        return $this->db->query("
        select chromosome from _temp_generation where komprob =
        (
        select max(komprob) as fit_max from _temp_generation
        )
        limit 1");
    }

}
