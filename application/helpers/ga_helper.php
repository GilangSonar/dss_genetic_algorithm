<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
/**
 * Created by PhpStorm.
 * User: patriciahadi
 * Date: 8/8/14
 * Time: 11:07 PM
 */

class individual
{
    var $fitness_value;
    var $gene_value;
    var $chrom;

    function set($g_val,$f_val,$c_val)
    {
        $this->fitness_value  = $f_val;
        $this->gene_value     = $g_val;
        $this->chrom          = $c_val;
    }

    function crossOver($p1,$p2)
    {
        $rNumber=rand(1,15);
        $this->gene_value = substr($p1->gene_value,0,$rNumber) . substr($p2->gene_value,$rNumber);
    }

    function mutate()
    {
        if(rand(1,5)==3)//this is only for probability , this function should not execute for all calls
        {
            $rNumber=rand(0,15);
            $this->gene_value[$rNumber]=1-$this->gene_value[$rNumber];
        }
    }
}

class GA
{
    var $population;			//initial population
    var $totalGeneration;
    var $arrayData;

    var $arr_individual;		//array
    var $arr_offspring;			//array
    var $currentGeneration;
    var $arr_avefitness ;		//for graph
    var $counter_arr_avefitness ;
    var $probalility;

    function GA()
    {
        $this->currentGeneration = 0;
        $this->counter_arr_avefitness = 0;
        $this->counter_arr_probability = 0;
    }


    function dataEmp()
    {
        $result = $this->arrayData;
        return ($result);
    }

    function shuffleData($arr){
        shuffle($arr);
        $len = count($arr);

        $array = array();
        for ($j=0; $j<$len;$j++){
            $array[$j]= $arr[$j] ;
        }
        return $array;
    }

    function fitness($data){
        $return = 0;
        $num_selected = count($data);
        for($i = 0; $i < $num_selected; $i++) {
            if($i < 2)
                $return += ($num_selected - $i) * $data[$i];
            else
                $return += $data[$i];
        }
        $fit = round($return /= $num_selected,2);
        return $fit;
    }

    function buildFirstGeneration()
    {
        for ($a=1 ; $a<=$this->population ; $a++)
        {
            $indiv = new individual();
            $g_value = $this->shuffleData($this->dataEmp());
            $f_value = $this->fitness($g_value);
            $indiv->set($g_value,$f_value,implode(",",$g_value));
            $this->arr_individual[$a]=$indiv;
        }
        $this->currentGeneration++;
    }

    function buildChilds()
    {
        for ($i=1 ; $i<=$this->population ; $i++)
        {
            $indiv = new individual();
            $parents = $this->selectParents();
            $indiv->crossOver($parents[0],$parents[1]);
            $indiv->mutate();
            $f_value=$this->fitness($indiv->gene_value);
            $indiv->set($indiv->gene_value,$f_value,implode(",",$indiv->gene_value));
            $this->arr_offspring[$i]=$indiv;
        }
        $this->currentGeneration++;
        $this->arr_individual = $this->arr_offspring;
    }

    function selectParents()
    {
        //sort the individual array
        for($i=0 ; $i<=$this->population ; $i++)
        {

            for($j=$i+1 ; $j<$this->population ; $j++)
            {
                if( $this->arr_individual[$i]->fitness_value > $this->arr_individual[$j]->fitness_value )
                {
                    $temp1 = $this->arr_individual[$i];
                    $this->arr_individual[$i] = $this->arr_individual[$j];
                    $this->arr_individual[$j] = $temp1;
                }
            }
        }
        //----------

        $randTopNumber=floor(lcg_value() * ($this->population / 2))+1;
        $randAllNumber=floor(lcg_value() * $this->population)+1;

        while($randAllNumber == $randTopNumber)
            $randAllNumber=floor(lcg_value() * $this->population)+1;

        $indiv1 = $this->arr_individual[$randTopNumber];
        $indiv2 = $this->arr_individual[$randAllNumber];

        return array($indiv1, $indiv2);
    }

    function debug()
    {
        $this->showHeader();
        $this->buildFirstGeneration();
        $this->showThisGeneration($this->arr_individual);

        for($i=($this->currentGeneration+1) ; $i<=($this->totalGeneration) ; $i++)
        {
            $this->buildChilds();
            $this->showThisGeneration($this->arr_individual);
        }
    }

    function showThisGeneration($arr_val)
    {
        $sumFitness = 0;
        for($i=1 ; $i<=$this->population ; $i++)
        {
            $sumFitness += $arr_val[$i]->fitness_value;
        }
        $averageFitness = $sumFitness / $this->population;

        //------- max and min fitness
        $max_fitness = $arr_val[1]->fitness_value;
        $min_fitness = $arr_val[1]->fitness_value;
        for($i=2 ; $i<=$this->population ; $i++)
        {
            if($max_fitness < $arr_val[$i]->fitness_value)
                $max_fitness = $arr_val[$i]->fitness_value;
            if($min_fitness > $arr_val[$i]->fitness_value)
                $min_fitness = $arr_val[$i]->fitness_value;
        }

        //for graph
        $this->arr_avefitness[$this->counter_arr_avefitness++]=$averageFitness;
        //end

        print ("<p>\n");
        print ("GENERATION " . $this->currentGeneration . "<br />");
        print ("Local fitness : max = " . $max_fitness . " , ave = " . $averageFitness . " , min = " . $min_fitness . " , Sum = " . $sumFitness . "<br />" );
        print ("<table width='35%' style='font-family:Tahoma;font-size:12px;'>\n");
        print ("<tr><td><b>No</b></td><td><b>Chromosome</b></td><td><b>Fitness</b></td><td><b>Probability</b></td><td><b> Com.Probability</b></td></tr>\n");
        for($i=1;$i<=$this->population;$i++)
        {
            // Probability ( fitness_value / sumFitness )
            $probability = round($arr_val[$i]->fitness_value / $sumFitness,4);

            if($i < 2 ){
                $komprob = $probability;
            }else{
                $komprob += $probability;
            }

            print ("<tr>");
            print ("<td>" . $i ."</td>");
            print ("<td>" . $arr_val[$i]->chrom . "</td>");
            print ("<td>" . $arr_val[$i]->fitness_value . "</td>");
            print ("<td>" . $probability . "</td>");
            print ("<td>" . $komprob . "</td>");
            print ("</tr>\n");
        }
        print ("</table>\n");
        print ("</p>\n");
    }

    function showHeader()
    {
        print("<hr width='70%' align='left'><b>Genetic Algorithm - DSS for choosing the best position of employees</b><br>");
        print("<b>BY : ANDREAS PUTRA HIRASNA </b><br />");
        print("<hr width='70%' align='left' /><br />");
    }


}