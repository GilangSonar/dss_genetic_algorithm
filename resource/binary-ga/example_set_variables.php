<?php
require_once('ga.class.php');

$fitness_function 		= $_POST['fitness_function'];
$generation				= $_POST['generation'];
$population 			= $_POST['population'];
$lower_bound 			= $_POST['lower_bound'];
$upper_bound 			= $_POST['upper_bound'];

$ga = new GA();

$ga->fitnessFunction	= $fitness_function;
$ga->totalGeneration 	= $generation;
$ga->population			= $population;
$ga->lowerBound			= $lower_bound;
$ga->upperBound			= $upper_bound ;
$ga->file_name			= 'ga_results.txt';

$ga->debug();


?>