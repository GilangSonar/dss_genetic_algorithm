<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Genetic Algorithm</title>
</head>

<body style="font-family:Tahoma;font-size:13px;background-color:#666666;" topmargin="100">
<form name="form1" method="post" action="example_set_variables.php">
<table align="center" bgcolor="#FFFFFF" dir="rtl" width="400" >
	<tr bgcolor="#CCCCCC" height="40"><td colspan="2" align="center"><b>Binary GA<b></td></tr>
	<tr height="20"><td colspan="2"></td></tr>
	<tr><td><input type="text" name="fitness_function" style="direction:ltr" value="2,-5,8" /></td><td dir="ltr" width="150">Fitness Function Indexes:</td></tr>
	<tr><td style="font-size:11px;color:#FF0000;direction:ltr" colspan="2">F(X) = A(n) X^n + A(n-1) X^(n-1) + ... + A(2) X^2 + A(1) X^1 + A(0) </td></tr>
	<tr><td style="font-size:11px;color:#FF0000;direction:ltr" colspan="2">example of Entery data : 2,-5,8  &nbsp;&nbsp;&nbsp;<font color="#0066FF">equal to : </font> F(X) = 2 X^2 - 5 X + 8 </td></tr>
	<tr><td><input type="text" name="generation" style="direction:ltr" value="100" /></td><td dir="ltr">Generation Number :</td></tr>
	<tr><td><input type="text" name="population" style="direction:ltr" value="10" /></td><td dir="ltr">Initial Population :</td></tr>
	<tr><td><input type="text" name="lower_bound" style="direction:ltr" value="0.0" /></td><td dir="ltr">Lower Bound :</td></tr>
	<tr><td><input type="text" name="upper_bound" style="direction:ltr" value="5.0" /></td><td dir="ltr">Upper Bound :</td></tr>
	<tr height="40"><td colspan="3" align="center"><input type="submit" name="submit" value="RUN" style="font-family:Tahoma;font-size:12px;color:#FF0000;width:100px;height:26px" /></td></tr>
	<tr height="20"><td colspan="3"></td></tr>
</table>
</form>

</body>
</html>
